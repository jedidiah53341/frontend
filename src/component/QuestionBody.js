import {useState} from 'react'
import QuestionTitle from './QuestionTitle';
import '../App.css';

function QuestionBody(props) {
    const [answered, setAnswered] = useState(props.answered);
            return (
            <div>
              <span>Current number : {props.number}</span><br/>
              <span>Passed number : {props.passed}</span>
            <div className={(props.info.id === props.number) ? 'display':'hide'} >
            <div className="q">

  
            <div className={props.passed > props.info.id ? "answered" : "unanswered"}>
            <QuestionTitle answered={answered} name={props.info.name} info={props.info}/>
            
            {  
            props.info.questions.map((question) =>
              <div>
                <input name={props.info.id} disabled={props.passed > props.info.id ? true:false} type={(props.info.type === 'select') ? 'checkbox' : 'radio'} />
                <label>{question}</label>
              </div>
                    )
            }

            </div>
          
            </div>
            
              </div>
              </div>
                );
                }
    
  export default QuestionBody;
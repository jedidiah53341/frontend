
import data from './data.json'
import QuestionBody from './component/QuestionBody';
import UseState from './component/UseState';
        
function App() {
  return (
    <div className="App">
      <UseState />
    </div>
  );
}

export default App;

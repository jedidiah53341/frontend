import {useState} from 'react'
import data from '../data.json'
import QuestionBody from './QuestionBody';
function App() {

  const [number, setNumber] = useState(1);
  const [answered, setAnswered] = useState(false);
  const [passedNumber,setPassedNumber] = useState(1);

  const count = Object.keys(data).length;

  const DisplayData=data.map(
    (info)=>{return(
    <div className="q" className={(info.id === number) ? 'display':'hide'} >
    <QuestionBody info={info} number={number} passed={passedNumber}/>
    </div>
    )})
  
    const nextQuestion = () => {
        setNumber(number+1);
        setAnswered(true);
        if (number == passedNumber) {
        setPassedNumber(passedNumber+1)
        }
      }
      const prevQuestion = () => {
        setNumber(number-1);
      }
  return (
    <div className="App">
    {DisplayData}

    { number != 1 && 
    <button  onClick={prevQuestion} >Previous</button> 
    }
    { number == count ? 
    <button>Finish</button> 
    :
    <button onClick={nextQuestion} >Next</button> 
    }
    </div>
  );
}

export default App;

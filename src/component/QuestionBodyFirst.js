import {useState} from 'react'
import RadioQuestion from './RadioQuestion';
import Question from './Question';

function QuestionBody(props) {
    const [answered, setAnswered] = useState(false);

    if (props.info.type === 'select') {
    return (
      <div className="Question">
          <h1>{props.info.name}</h1>

          {  
            props.info.questions.map((question) =>
                <Question name={question} answered={answered}/>
                    )
            }

        <button onClick={()=>setAnswered(true)}>Previous</button>
        <button onClick={()=>setAnswered(true)}>Next</button>
      </div>
        );
        }

        if (props.info.type === 'radio') {
            return (
              <div className="Question">
                  <h1>{props.info.name}</h1>
        
                  { 
                    props.info.questions.map((question) =>
                        <RadioQuestion name={question} answered={answered} />
                            )
                    }
            <button onClick={()=>setAnswered(true)}>Previous</button>
            <button onClick={()=>setAnswered(true)}>Next</button>
              </div>
                );
                }

    }
    

  export default QuestionBody;